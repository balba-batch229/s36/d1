const express = require("express");
const router = express.Router();
const taskControllers = require("../controllers/taskControllers.js");

router.get ("/",(req, res) =>{

	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController));
});

router.post("/",(req, res) =>{
	taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController));

});

router.delete("/:id", (req, res) => {
	taskControllers.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

router.put("/:id", (req, res)=>{
	taskControllers.updateTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController));
});


router.put("/:id/complete", (req, res)=>{
	taskControllers.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));

module.exports = router;