const mongoose = require("mongoose");
const express = require("express");
const taskRoutes = require("./routes/taskRoute.js");

const app = express();
const port = 4001;

app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use("/tasks", taskRoutes);
app.use("/profile", taskRoutes);

mongoose.connect("mongodb+srv://admin:admin123@zuitt.cvgjjmq.mongodb.net/B229_to-do?retryWrites=true&w=majority",{
	useNewUrlParser : true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));


app.listen(port, () => console.log(`Now listening to port ${port}.`));

